using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using CavemanTools.Infrastructure;
using MonolithBus;
using Shouldly;
using Xunit;
using Xunit.Abstractions;

namespace Tests
{
    
    public class UnitTest1:IDisposable
    {
        private readonly ITestOutputHelper _h;
        private IDispatchMessages _bus;

        public UnitTest1(ITestOutputHelper h)
        {
            _h = h;
            _bus = Setup.Bus(h).GetDispatcher();
        }
        [Fact]
        public async Task Normal_usage()
        {
            
            await _bus.Send(new DoStuff()).ConfigureFalse();
            await _bus.Publish(new PositionOpened(){TransactionId = Guid.NewGuid()}).ConfigureFalse();
            await Task.Delay(200.ToMiliseconds());
            Result.Instance.Contains("DoStuff").ShouldBeTrue();
            Result.Instance.Contains("Pos").ShouldBeTrue();
        }


        [Fact]
        public async Task executing_request()
        {
            var rez = await _bus.Request(new SomeReq());
            rez.Data.ContainsKey("ok").ShouldBeTrue();
        }

        [Fact]
        public async Task saga_ok()
        {
            var gid = Guid.NewGuid();
            await _bus.Publish(new PositionOpened(){TransactionId = gid});
            await Task.Delay(100.ToMiliseconds());
            await _bus.Publish(new CashOutFromTransaction(){TransactionId = gid});
            await Task.Delay(500.ToMiliseconds());
            Result.Instance.Contains("After").ShouldBeTrue();
        }


        [Fact]
        public async Task saga_not_found()
        {
            await _bus.Publish(new CashOutFromTransaction(){TransactionId = Guid.NewGuid()});
            await Task.Delay(200.ToMiliseconds()); 
        }

        [Fact]
        public async Task recover_msgs()
        {
            await ServiceBus.Instance.RecoverUnhandled().ConfigureFalse();
            await Task.Delay(500.ToMiliseconds());
        }

        public void Dispose()
        {
           
        }
    }

    public class AfterIncreasingPos : ASaga
    {
        private readonly IDispatchMessages _dispatcher;

        public AfterIncreasingPos(IDispatchMessages dispatcher)
        {
            _dispatcher = dispatcher;
            MapCorrelationIdFrom<PositionOpened>(e=>e.TransactionId);
            MapCorrelationIdFrom<CashOutFromTransaction>(e=>e.TransactionId);
            SagaCompletesWhen(()=>Contains("cash"),()=>dispatcher.Send(new AfterSaga()));
            
        }

      

        [StartsSaga]
        public Task Handle(PositionOpened pos)
        {
            Mark("pos");
            return Task.CompletedTask;
        }

        public Task Handle(CashOutFromTransaction ev)
        {
            Mark("cash");
            return Task.CompletedTask;
        }


    }

    public class AfterSaga : AMessage
    {

    }


    public class Result:List<string>
    {
        public static Result Instance = new Result();
    }


    public class DoStuff : AMessage
    {

    }

    [MessageHandler]
    public class CommandHandler
    {
        public Task Execute(DoStuff cmd)
        {
            Result.Instance.Add("DoStuff");
            return Task.CompletedTask;
        }

        public Task Execute(AfterSaga s)
        {
            Result.Instance.Add("After");
            return Task.CompletedTask;
        }

        public Task Handle(PositionOpened ev)
        {
            Result.Instance.Add("Pos");
            return Task.CompletedTask;
        }

        public Task<CommandResult> Execute(SomeReq c)
        {
            var result = new CommandResult();
            result["ok"] = true;
            return Task.FromResult(result);
        }
    }


    public class SomeReq:AMessage
    {

    }

   public class PositionOpened:AMessage
    {
        public Guid TransactionId { get; set; }
    }

    public class CashOutFromTransaction:AMessage
    {
        public Guid TransactionId { get; set; }
    }
}
