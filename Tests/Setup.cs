using System;
using System.Data.SQLite;
using System.Reflection;
using Autofac;
using MonolithBus;
using Serilog;
using SqlFu;
using Xunit.Abstractions;

namespace Tests
{
    public class Setup
    {
        public static ILogger Logger(ITestOutputHelper h)
        {
            return new LoggerConfiguration().MinimumLevel.Debug().WriteTo.TestOutput(h).CreateLogger();
        }

        public static readonly bool IsAppVeyor = Environment.GetEnvironmentVariable("Appveyor")?.ToUpperInvariant() == "TRUE";

        public static string SqliteConnectionString { get; } = "Data Source=test.db;Version=3;New=True;BinaryGUID=False";
        


        public static IContainer Autofac()
        {
            var cb=new ContainerBuilder();
            cb.Register(_ => ServiceBus.CreateDispatcher()).AsSelf();
            cb.RegisterAssemblyTypes(typeof(Setup).Assembly).AsSelf().AsImplementedInterfaces();
            return cb.Build();
        }

        public static ServiceBus Bus(ITestOutputHelper h)
        {
            var fac = Autofac();
            var logger = Logger(h);
            
            var s= ServiceBus.Configure(c =>
            {
                c.WithLogger(logger).DiscoverHandlers(Assembly.GetExecutingAssembly())
                    .UseSqlite(SQLiteFactory.Instance.CreateConnection, SqliteConnectionString)
                    .ResolveTypesWith(t => fac.Resolve(t));
            });
            SqlFuManager.Configure(c => { c.OnException = (cmd, ex) => logger.Error(cmd.FormatCommand(), ex); });
            return s;
        }
    }
}