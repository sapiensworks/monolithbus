﻿using System;
using System.Threading.Tasks;

namespace MonolithBus
{
    public class SagaBehaviours:HandlerExecutorArgs
    {
        private readonly IStoreSagaState _store;
        private readonly ASaga _handler;
        private readonly IIdentifyMessage _evnt;
        private readonly bool _startSaga;
        private string _id;
        private SagaState _state= new SagaState();

        public SagaBehaviours(IStoreSagaState store,ASaga handler, IIdentifyMessage evnt,bool startSaga=false):base(handler,evnt)
        {
            _store = store;
            _handler = handler;
            _evnt = evnt;
            _startSaga = startSaga;
            _id=handler.GetCorrelationId(evnt);
        }



        public Task SaveState()
        {
            if (_handler.IsCompleted)
            {
                
                return _store.Delete(_id);
            }
            _state.Id = _id;
            _state.Data=_handler.RawData;
            return _store.Save(_state, _startSaga);
        }

        public async Task LoadSaga()
        {
            if (_startSaga)  return;
            _state = await _store.GetSaga(_id);
            if (_state==null) throw  new SagaNotFoundException(_handler.GetType(),_evnt);
            _handler.RawData=_state.Data;
        }

    }
}