﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using CavemanTools;
using Serilog;
using Serilog.Core;

namespace MonolithBus
{
    public class ServiceBus:IConfigureServiceBus
    {
        private IStoreMessages _storage;
        internal static ILogger Log { get; set; }= Serilog.Core.Logger.None;

        public static ServiceBus Instance
        {
            get
            {
                if (_instance == null) throw new InvalidOperationException("Bus hasn't been configured");
                return _instance;
            }
        }

        private QueueingService _queues;
        private static ServiceBus _instance;
        /// <summary>
        /// Used this to register <see cref="IDispatchMessages"/> with a DI Container
        /// </summary>
        /// <returns></returns>
        public static IDispatchMessages CreateDispatcher()
        {
            _instance.MustNotBeNull();
            return _instance.GetDispatcher();
        }

        private ServiceBus()
        {
            
        }

        public IDispatchMessages GetDispatcher()=>new Dispatcher(_storage,_storage,_queues);
        
        public static ServiceBus Configure(Action<IConfigureServiceBus> cfg)
        {
            _instance=new ServiceBus();
            cfg(_instance);
            _instance.Build();
            return _instance;
        }

        private HandlersAndMessagesDiscovery _discovery=new HandlersAndMessagesDiscovery();
        private Action<IIdentifyMessage> _relay=Empty.ActionOf<IIdentifyMessage>();
        private HandlerExecutorFactory _factory;

        /// <summary>
        /// Recover and process unhandled messages
        /// </summary>
        /// <returns></returns>
        public Task RecoverUnhandled()
        {
            Log.Information("Recovering unhandled messages");
            return _queues.RecoverUnhandledMessages();
        }

        private void Build()
        {
            _storage.MustNotBeNull();
            _factory.MustNotBeNull(); 
             Log.Information("Building monolith bus");
            Log.Information($"Storage provider is implemented by {_storage.GetType()}");
            _queues=new QueueingService(_discovery,_factory,_storage,_relay);
        }

        public IConfigureServiceBus WithStorage(IStoreMessages storage)
        {
            _storage = storage;
            return this;
        }

        public IConfigureServiceBus WithLogger(ILogger log)
        {
            log.MustNotBeNull();
            Log = log;
            return this;
        }

        public IConfigureServiceBus ResolveTypesWith(Func<Type, object> factory)
        {
            factory.MustNotBeNull();
            _factory = new HandlerExecutorFactory(factory,()=>_storage);
            return this;

        }

        public IConfigureServiceBus RelayEventsWith(Action<IIdentifyMessage> relay)
        {
            _relay = relay;
            return this;
        }

        public IConfigureServiceBus DiscoverHandlers(IEnumerable<Type> types)
        {
            _discovery.DiscoverItems(types.Where(t=>t.HasCustomAttribute<MessageHandlerAttribute>() || t.DerivesFrom<ASaga>()).ToArray());
            return this;
        }
    }

    public interface IConfigureServiceBus
    {
        IConfigureServiceBus WithStorage(IStoreMessages storage);
        IConfigureServiceBus WithLogger(ILogger log);
        IConfigureServiceBus ResolveTypesWith(Func<Type,object> factory);
        /// <summary>
        /// An event marked with <see cref="RelayAttribute"/> will be automatically sent to the relay.
        /// Used to communicate events to the UI
        /// </summary>
        /// <param name="relay"></param>
        /// <returns></returns>
        IConfigureServiceBus RelayEventsWith(Action<IIdentifyMessage> relay);
        IConfigureServiceBus DiscoverHandlers(IEnumerable<Type> types);
    }
}