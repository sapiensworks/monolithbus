﻿using System;
using System.Threading.Tasks;
using CavemanTools.Infrastructure;

namespace MonolithBus
{
    public abstract class AHandlerExecutor:IMessageHandlerExecutor
    {
        protected AHandlerExecutor(HandlerExecutorArgs data)
        {
            Data = data;
        }

        public abstract Task Execute();
        public HandlerExecutorArgs Data { get; private set; }
        public Optional<CommandResult> Result { get; protected set; }
    }
}