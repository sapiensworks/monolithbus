﻿using System;

namespace MonolithBus
{
    public class BusConfigurationException : Exception
    {
        public BusConfigurationException(string toFormat):base(toFormat)
        {
        
        }
    }

    public class MonolithBusException : Exception
    {
        public MonolithBusException(string toFormat):base(toFormat)
        {
        
        }
    }
    public class MonolithBusStorageException : Exception
    {
        public MonolithBusStorageException(Exception inn) : base("", inn)
        {
        
        }
    }


}