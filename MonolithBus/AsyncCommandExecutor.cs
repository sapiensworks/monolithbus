﻿using System;
using System.Threading.Tasks;
using CavemanTools.Infrastructure;

namespace MonolithBus
{
    public class AsyncCommandExecutor:AHandlerExecutor
    {
        private bool _isRequest;

        public AsyncCommandExecutor(HandlerExecutorArgs data,bool isRequest=false) : base(data)
        {
            _isRequest = isRequest;
        }

        public override async Task Execute()
        {
            if (!_isRequest)
            {
                await Data.Handler.Execute((dynamic) Data.Message);
                return;
            }
            var rez=await Data.Handler.Execute((dynamic) Data.Message);
            Result=new Optional<CommandResult>(rez as CommandResult);

        }
    }
}