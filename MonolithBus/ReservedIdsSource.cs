﻿using System;

namespace MonolithBus
{
    public class ReservedIdsSource
    {
        public Type HandlerType { get; set; }
        public Guid MessageId { get; set; }

        public string GetId() => $"{MessageId}{HandlerType.FullName}";
        //  public int Count { get; set; }
    }
}