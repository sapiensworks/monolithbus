﻿using System;
using System.Data.Common;
using System.Reflection;
using MonolithBus.Providers;
using Serilog;
using SqlFu;
using SqlFu.Providers;
using SqlFu.Providers.Sqlite;
using SqlFu.Providers.SqlServer;

namespace MonolithBus
{
    public static class Extensions
    {
        public static ILogger Log<T>(this T inst) => ServiceBus.Log.ForContext<T>();

        public static IConfigureServiceBus DiscoverHandlers(this IConfigureServiceBus c, Assembly asm = null)
        {
            asm = asm ?? Assembly.GetCallingAssembly();
            c.DiscoverHandlers(asm.ExportedTypes);
            return c;
        }

        /// <summary>
        /// Use sqlite storage
        /// </summary>
        /// <param name="cf"></param>
        /// <param name="cnxFactory"></param>
        /// <param name="cnxString"></param>
        /// <returns></returns>
        public static IConfigureServiceBus UseSqlite(this IConfigureServiceBus cf, Func<DbConnection> cnxFactory,
            string cnxString)
        {
            SqlStorage.SetupSqlFu(new SqliteProvider(cnxFactory),cnxString);
            var storage = new SqlStorage(SqlFuManager.GetDbFactory("monolithBus"));
            cf.WithStorage(storage);
           storage.InitSqlite();
            return cf;
        }


        //public static IConfigureServiceBus UseSqlServer(this IConfigureServiceBus cf, Func<DbConnection> cnxFactory,
        //    string cnxString)
        //{
        //    SqlStorage.SetupSqlFu(new SqlServer2012Provider(cnxFactory), cnxString);
        //    cf.WithStorage(new SqlStorage(SqlFuManager.GetDbFactory("monolithBus")));
        //    return cf;
        //}
    }
}