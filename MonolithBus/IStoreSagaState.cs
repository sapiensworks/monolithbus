﻿using System.Threading.Tasks;

namespace MonolithBus
{
    /// <summary>
    /// Used as a singleton.
    /// If an user defined repository is configured, it will be used insted of this.
    /// </summary>
    public interface IStoreSagaState
    {
        ///  <summary>
        /// 
        ///  </summary>
        /// <param name="correlationId">Unique saga state id</param>
        /// <exception cref="SagaNotFoundException"></exception>
        /// <exception cref="MonolithBusStorageException"></exception>
        /// <returns></returns>
        Task<SagaState> GetSaga(string correlationId);

        /// <summary>
        /// 
        /// </summary>
        /// <exception cref="SagaConcurrencyException"></exception>
        /// <exception cref="MonolithBusStorageException"></exception>
        /// <param name="state"></param>   
        /// <param name="isNew">True if the saga just started</param>
        Task Save(SagaState state, bool isNew=false);

        /// <summary>
        /// 
        /// </summary>
        /// <exception cref="MonolithBusStorageException"></exception>
        /// <param name="id"></param>
        /// <returns></returns>
        Task Delete(string id);

    }
}