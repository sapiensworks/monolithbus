﻿using System;
using System.Threading.Tasks;
using CavemanTools.Infrastructure;

namespace MonolithBus
{
    public interface IMessageHandlerExecutor
    {
        Task Execute();
        HandlerExecutorArgs Data { get; }
        Optional<CommandResult> Result { get; }
    }
}