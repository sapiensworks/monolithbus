﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MonolithBus
{
    /// <summary>
    /// sinlgeton
    /// </summary>
    public class QueueingService
    {
        private readonly HandlersAndMessagesDiscovery _cfg;
        private readonly IStoreMessages _store;

        MessageQueue _commandsQ;
        MessageQueue _eventsQ;
        public QueueingService(HandlersAndMessagesDiscovery cfg, HandlerExecutorFactory fac, IStoreMessages store,
            Action<IIdentifyMessage> relay)
        {
            _cfg = cfg;
            _store = store;

            Func<IIdentifyMessage, MessageProcessor> func = m=>new MessageProcessor(m,_store,cfg.GetHandlers(m.GetType()).Select(h=>HandlersAndMessagesDiscovery.GetExecutor(fac,h,m)).ToArray(),ServiceBus.Log.ForContext<MessageProcessor>(),relay);
            _commandsQ=new MessageQueue(func,store);
            _eventsQ=new MessageQueue(func,store);
        }
        public void QueueCommand(IIdentifyMessage msg)
        {
            this.Log().Debug("Queueing command {messageType}-{messageId}",msg.GetType(),msg.MessageId);
            _commandsQ.Add(msg);
        }

        public async Task RecoverUnhandledMessages()
        {
            var msg = await _store.GetUnhandledMessages().ConfigureFalse();
            if (msg.IsNullOrEmpty())
            {
                this.Log().Debug("No message to be recovered. Skipping recovery.");
                return;
            }

            this.Log().Information($"Found {msg.Count()} unhandled messages");

            foreach (var m in msg)
            {
                if (_cfg.IsCommand(m))
                {
                    QueueCommand(m);
                }
                else
                {
                    QueueEvent(m);
                }
            }
        }
            

        public MessageProcessor GetCommandProcessor(IIdentifyMessage msg) => _commandsQ.GetCommandExecutor(msg);

        public void QueueEvent(IIdentifyMessage msg)
        {
            this.Log().Debug("Queueing event {messageType}-{messageId}",msg.GetType(),msg.MessageId);
            _eventsQ.Add(msg);
        }
    }
}