﻿using System;
using System.Collections.Concurrent;
using System.Linq;
using System.Threading.Tasks;

namespace MonolithBus
{
    public class MessageQueue
    {
        private readonly Func<IIdentifyMessage, MessageProcessor> _getProcessor;
        private readonly IStoreMessages _store;
        ConcurrentQueue<IIdentifyMessage> _msgs=new ConcurrentQueue<IIdentifyMessage>();
        private Task _task=Task.CompletedTask;
        private object _sync = new object();

        public MessageQueue(Func<IIdentifyMessage,MessageProcessor> getProcessor,IStoreMessages store)
        {
            _getProcessor = getProcessor;
            _store = store;
        }

        public void Add(IIdentifyMessage msg)
        {
          lock (_sync)
            {
                if (_msgs.ToArray().Any(m=>m.MessageId==msg.MessageId)) return;
                _msgs.Enqueue(msg);
                if (!_task.IsCompleted) return;
                _task = Task.Run(Process).ContinueWith(AfterTaskFinishes);
            }

        }

        private void AfterTaskFinishes(Task obj)
        {
            if (obj.IsFaulted) this.Log().Error(obj.Exception,"Message processor threw unhandled  exception");
        }

        public MessageProcessor GetCommandExecutor(IIdentifyMessage msg)
        =>_getProcessor(msg);

        async Task Process()
        {
            begin:
            if(!_msgs.TryDequeue(out var msg)) return;
            await _getProcessor(msg).Process().ConfigureFalse();
            goto begin;
        }
    }
}