﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace MonolithBus
{
    /// <summary>
    /// Implementation must be thread safe, it will be used as a singleton
    /// </summary>
    public interface IStoreMessages : IAddMessageToProcessorStorage,IStoreSagaState,IStoreReservedMessagesIds
    {
        /// <summary>
        /// 
        /// </summary>
        /// <exception cref="MonolithBusStorageException"></exception>
        Task<IEnumerable<IIdentifyMessage>> GetUnhandledMessages();


        /// <summary>
        /// 
        /// </summary>
        /// <exception cref="MonolithBusStorageException"></exception>
        /// <param name="id"></param>
        Task MarkMessageHandled(Guid id);

        ///
        /// <exception cref="MonolithBusStorageException"></exception>
        Task MarkMessageFailed(Guid id);


    }
}