﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Security.Principal;
using System.Threading.Tasks;
using CavemanTools;

namespace MonolithBus
{
    public abstract class ASaga
    {
        /// <summary>
        /// Gets the correlation id for saga based on the mapped event property and value.
        ///  </summary>
        /// <exception cref="BusConfigurationException"></exception>
        /// <exception cref="NullReferenceException"></exception>
        /// <param name="evnt"></param>
        /// <returns></returns>
        internal string GetCorrelationId(IIdentifyMessage evnt)
        {
            evnt.MustNotBeNull();


            PropertyInfo pi = null;
            var etype = evnt.GetType();
            if (!_maps.TryGetValue(etype, out pi))
            {
                throw new BusConfigurationException(
                    "No mapping done for event '{0}'. Call CorrelationIdFor<> in the constructor".ToFormat(evnt.GetType().FullName));
            }

            var value = evnt.GetPropertyValue(pi.Name);

            if (value == null || (pi.PropertyType.IsValueType() && value.Equals(pi.PropertyType.GetDefault())))
            {
                throw new NullReferenceException("'{0}' property of '{1}' must have a non-default value".ToFormat(pi.Name, etype));
            }

            return FormatId(value.ToString());
        }

     

        private readonly Dictionary<Type, PropertyInfo> _maps = new Dictionary<Type, PropertyInfo>();

      

        private string FormatId(string value) => $"{value}-{GetType().FullName}";


        /// <summary>
        /// Which event property will be used to correlate with the saga
        /// </summary>
        /// <typeparam name="E"></typeparam>
        /// <param name="eventProperty"></param>
        protected void MapCorrelationIdFrom<E>(Expression<Func<E, object>> eventProperty) where E : IIdentifyMessage
        {
            _maps[typeof (E)] = eventProperty.GetPropertyInfo() as PropertyInfo;  
        }
        
        public Dictionary<string, object> RawData { get; internal set; }=new Dictionary<string, object>();

        protected async void MarkComplete()
        {
            IsCompleted = true;
            await _onComplete();
        }

        //public Guid Id { get; private set; } = Guid.NewGuid();
        public bool IsCompleted { get;  private set; }

        /// <summary>
        /// Sets a saga state value to be persisted
        /// Checks for saga completion
        /// </summary>
        /// <param name="key"></param>
        /// <param name="value"></param>
        protected void Set(string key, object value)
        {
            RawData[key] = value;
            if (_completionCondition()) MarkComplete();
        }

        /// <summary>
        /// Sets a true flag for <paramref name="key"/>
        /// Checks for saga completion
        /// </summary>
        /// <param name="key"></param>
        protected void Mark(string key)
        {
            RawData[key] = true;
            if (_completionCondition()) MarkComplete();
        }

        /// <summary>
        /// True if all specified keys are present
        /// </summary>
        /// <param name="keys"></param>
        /// <returns></returns>
        protected bool Contains(params string[] keys)
            => keys.All(k => RawData.ContainsKey(k));
      

        private Func<bool> _completionCondition = Empty.Func<bool>();
        private Func<Task> _onComplete=Empty.Func<Task>();

        public void SagaCompletesWhen(Func<bool> when,Func<Task> onComplete=null) 
        {
            when.MustNotBeNull();
            _completionCondition = when;
            _onComplete = onComplete ??Empty.Func<Task>();
        }
    }

    public class SagaState 
    {
        public SagaState()
        {
            
        }

        public string Id { get; set; }
        public Dictionary<string, object> Data { get; set; }
        public int Version { get; set; }
    }

   
}