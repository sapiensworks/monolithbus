﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using CavemanTools;
using CavemanTools.Infrastructure;
using Serilog;

namespace MonolithBus
{
    public class MessageProcessor
    {
        private readonly IIdentifyMessage _msg;
        private readonly IStoreMessages _store;
        private readonly IMessageHandlerExecutor[] _executors;
        private readonly ILogger _log;
        private Action<IIdentifyMessage> _relay=Empty.ActionOf<IIdentifyMessage>();


        public MessageProcessor(IIdentifyMessage msg,IStoreMessages store,IMessageHandlerExecutor[] executors,ILogger log,Action<IIdentifyMessage> relay)
        {
            _msg = msg;
            _store = store;
            _executors = executors;
            _log = log;
            
            var isEv = executors.FirstOrDefault();
            if (isEv?.Data.ShouldBeRelayed == true) _relay = relay;
        }

        public async Task<Optional<CommandResult>> Process()
        {
            var res = Optional<CommandResult>.Empty;
            if (_executors.IsNullOrEmpty())
            {
                _log.Debug("No handlers found for {messageType}-{messageId}. Skipping.",_msg.GetType(),_msg.MessageId);
                _relay(_msg);
                return res;
            }            
            
            var err = false;
            var sw = new Stopwatch();
            try
            {
                
                foreach (var ex in _executors)
                {
                   
                    var hType = ex.Data.Handler.GetType();
                    try
                    {
                        _log.Debug("Starting handling of {messageType}-{messageId} with {handlerType}",_msg.GetType(),_msg.MessageId,hType);
                        sw.Restart();
                        await ex.Execute().ConfigureFalse();
                        sw.Stop();
                        res = ex.Result;
                        _log.Debug("Handling of {messageType}-{messageId} with {handlerType} completed successfully in "+sw.ElapsedMilliseconds+"ms",_msg.GetType(),_msg.MessageId,hType);
                    }

                    catch (Exception e)when (!(e is OutOfMemoryException))
                    {
                        _log.Error(e, "Exception handling {messageType}-{messageId} with {handlerType}",_msg.GetType(), _msg.MessageId,
                            hType);
                        await _store.MarkMessageFailed(_msg.MessageId).ConfigureFalse();
                        err = true;
                    }
                    catch (SagaNotFoundException se)
                    {
                        _log.Error(se, "Saga not found for {handlerType} handling {messageType}-{messageId} with ",hType,_msg.GetType(), _msg.MessageId);
                        await _store.MarkMessageFailed(_msg.MessageId).ConfigureFalse();
                        err = true;
                    }
                }

                if (!err)await _store.MarkMessageHandled(_msg.MessageId).ConfigureFalse();
               
                _relay(_msg);
                return res;
            }
            catch (MonolithBusStorageException ex)
            {
                this.Log().Fatal(ex,"Service bus storage error");
                throw;
            }
        }
    }
}