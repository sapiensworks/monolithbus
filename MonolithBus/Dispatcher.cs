﻿using System;
using System.Linq;
using System.Threading.Tasks;
using CavemanTools.Infrastructure;

namespace MonolithBus
{
    public class Dispatcher : IDispatchMessages
    {
        private readonly IAddMessageToProcessorStorage _store;
        private readonly IStoreReservedMessagesIds _idemStore;
        private readonly QueueingService _svc;
        private readonly HandlersAndMessagesDiscovery _cfg;

        public Dispatcher(IAddMessageToProcessorStorage store,IStoreReservedMessagesIds idemStore,QueueingService svc)
        {
            _store = store;
            _idemStore = idemStore;
            _svc = svc;        
        }

        public async Task Publish(params IIdentifyMessage[] events)
        {
            await _store.Add(events).ConfigureFalse();
            foreach (var m in events)
            {
                _svc.QueueEvent(m);               
            }
        }

        public async Task<CommandResult> Request(IIdentifyMessage cmd)
        {
            await _store.Add(cmd).ConfigureFalse();
            var ex = _svc.GetCommandProcessor(cmd);
            var r=await ex.Process().ConfigureFalse();
            return r.Value;
        }

        public async Task<Guid[]> ReserveIdsFor<T>(T handlerType, Guid msgId, int howMany)
        {
            var data=new ReservedIdsSource();
            data.HandlerType = handlerType.GetType();
            data.MessageId = msgId;

            var guids = await _idemStore.Get(data).ConfigureFalse();
            if (guids.Length != 0) return guids;
            guids = Enumerable.Range(1, howMany).Select(_ => Guid.NewGuid()).ToArray();
            await _idemStore.Add(data, guids).ConfigureFalse();
            return guids;
        }

        public async Task Send(params IIdentifyMessage[] cmd)
        {
            await _store.Add(cmd).ConfigureFalse();
            foreach (var m in cmd) _svc.QueueCommand(m);
        }
    }
}