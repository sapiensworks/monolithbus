﻿using System;

namespace MonolithBus
{
    public class HandledMessageException : Exception
    {
        public Type HandlerType { get; set; }

        public HandledMessageException(Type handlerType,IIdentifyMessage msg,Exception inner):base("Handling message {0} with handler {1} threw exception".ToFormat(msg.MessageId,handlerType),inner)
        {
            HandlerType = handlerType;
        }
    }
}