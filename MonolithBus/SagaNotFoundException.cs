﻿using System;

namespace MonolithBus
{
    public class SagaNotFoundException:Exception
    {
        public Type Handler { get; }

        public SagaNotFoundException(Type handler,IIdentifyMessage msg):base($"Couldn't find saga state for {handler} handling {msg.GetType()} with id:'{msg.MessageId}")
        {
            this.Handler = handler;
            MessageInstance = msg;
        }

        public IIdentifyMessage MessageInstance { get; }
    }
}