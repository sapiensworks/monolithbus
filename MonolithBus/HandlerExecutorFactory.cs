﻿using System;

namespace MonolithBus
{
    /// <summary>
    /// singleton
    /// </summary>
    public class HandlerExecutorFactory
    {
        private readonly Func<Type, object> _factory;
        private readonly Func<IStoreSagaState> _sagaStore;

        public HandlerExecutorFactory(Func<Type,object> factory,Func<IStoreSagaState> sagaStore)
        {
            _factory = factory;
            _sagaStore = sagaStore;
        }

        public IMessageHandlerExecutor Create(CommandHandlerInfo info,IIdentifyMessage msg)
        {
            var inst = _factory(info.HandlerType);
            var args=new HandlerExecutorArgs(inst,msg);
            return new AsyncCommandExecutor(args,info.IsRequest);
        }
        
        public IMessageHandlerExecutor Create(EventHandlerInfo info,IIdentifyMessage msg)
        {
            var inst = _factory(info.HandlerType);
            if (!info.IsSaga) return new AsyncEventExecutor(new HandlerExecutorArgs(inst, msg));

            var sb=new SagaBehaviours(_sagaStore(),inst as ASaga,msg,info.StartsSaga);
            return new AsyncEventExecutor(sb);
        }


    }
}