﻿using System.Threading.Tasks;

namespace MonolithBus
{
    public class AsyncEventExecutor : AHandlerExecutor
    {
       
        private readonly SagaBehaviours _saga;


        public AsyncEventExecutor(SagaBehaviours inst):base(inst)
        {
            _saga = inst;
        }
        public AsyncEventExecutor(HandlerExecutorArgs inst):base(inst)
        {
            
        }
        public override async Task Execute()
        {
            begin:
            try
            {
            if(_saga!=null) await _saga.LoadSaga();
            await Data.Handler.Handle((dynamic) Data.Message);
            if(_saga!=null) await _saga.SaveState();
            }           

            catch (SagaConcurrencyException)
            {
                goto begin;
            }
        }
      

    }
}