﻿using System;
using System.Linq;

namespace MonolithBus
{
    /// <summary>
    /// singleton
    /// </summary>
    public class HandlersAndMessagesDiscovery
    {
        private MessageHandlerInfo[] _handlerInfos;

        public void DiscoverItems(Type[] types)
        {
            _handlerInfos = types.SelectMany(d => d.GetMethods())
                .Select(MessageHandlerInfo.GetHandlerInfo)
                .Where(d => d.HasValue)
                .Select(d => d.Value)
                .ToArray();        

            _handlerInfos.ForEach(h=>this.Log().Debug("Found handler {handlerType} for message {messageType} ({messageCategory})",h.HandlerType,h.MessageType,h.Category));
            var msgLength = _handlerInfos.GroupBy(d => d.MessageType).Count();
            this.Log().Information($"Discovered {msgLength} messages {_handlerInfos.Length} handlers");
        }


        public bool IsCommand(IIdentifyMessage msg)
        {
            var hi = GetHandlers(msg.GetType()).First();
            return hi.Category==MessageType.Command;
        }

        public MessageHandlerInfo[] GetHandlers(Type msgType) => _handlerInfos.Where(d => d.MessageType == msgType).ToArray();

        public static IMessageHandlerExecutor GetExecutor(HandlerExecutorFactory fac, MessageHandlerInfo mi,IIdentifyMessage msg)
        {
            switch (mi)
            {
                case CommandHandlerInfo ci: return fac.Create(ci, msg);
                case EventHandlerInfo ei: return fac.Create(ei, msg);
            }
            throw new InvalidOperationException();
        }

    }
}