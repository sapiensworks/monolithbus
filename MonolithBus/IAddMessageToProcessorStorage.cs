﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace MonolithBus
{
    public interface IAddMessageToProcessorStorage
    {
        /// <summary>
        /// If message exists ignore it
        /// </summary>
        /// <param name="items"></param>
        Task Add(params IIdentifyMessage[] items);
    }
}