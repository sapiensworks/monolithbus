﻿using System.Reflection;

namespace MonolithBus
{
    public class HandlerExecutorArgs
    {
        public HandlerExecutorArgs(dynamic handler, IIdentifyMessage message)
        {
            Handler = handler;
            Message = message;
            ShouldBeRelayed = message.GetType().HasCustomAttribute<RelayAttribute>();
        }

        public dynamic Handler { get;  }
        public IIdentifyMessage Message { get;}
        public bool ShouldBeRelayed { get;}
    }
}