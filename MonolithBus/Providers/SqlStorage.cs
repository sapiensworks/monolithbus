﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using SqlFu;
using SqlFu.Providers;
using Utf8Json;

namespace MonolithBus.Providers
{

    public class MessagesRow
    {
        public long Id { get; set; }
        public Guid MessageId { get; set; }
        public string Data { get; set; }
        public string MessageType { get; set; }
        public bool IsPoison { get; set; }
    }

    public class SagasRow
    {
        public string Id { get; set; }
        public string Data { get; set; }
        public int Version { get; set; }
    }

    public class ReservedIdsRow
    {
        public string SourceId { get; set; }
        public string Ids { get; set; }
    }

    public class SqlStorage:IStoreMessages
    {
        private readonly IDbFactory _getDb;

        public static string PackMessage(IIdentifyMessage msg)

            => JsonSerializer.ToJsonString(msg);


        public static IIdentifyMessage UnpackMessage(string data,Type msgType)
        {
            return JsonSerializer.NonGeneric.Deserialize(msgType, data) as IIdentifyMessage;
        }

        public SqlStorage(IDbFactory getDb)
        {
           
            _getDb = getDb;
        }

        internal void Init(string sql)
        {
            using (var db = _getDb.Create())
            {
                db.AddDbObjectOrIgnore(sql);
            }
        }

        public async Task Add(params IIdentifyMessage[] items)
        {
            this.Log().Debug("Adding messages to storage");
            try
            {
                using (var db = await _getDb.CreateAsync(CancellationToken.None))
                {
                    using (var t = db.BeginTransaction())
                    {
                        foreach (var m in items)
                        {
                            await db.InsertIgnoreAsync(new MessagesRow()
                            {
                                MessageId = m.MessageId, Data = PackMessage(m), MessageType = m.GetType().AssemblyQualifiedName
                            }).ConfigureFalse();
                        }

                        t.Commit();
                        this.Log().Debug($"{items.Length} messages added to storage");
                    }
                }
            }
            catch (DbException ex)
            {
                throw new MonolithBusStorageException(ex);
            }
            
        }

        public async Task<SagaState> GetSaga(string correlationId)
        {
            this.Log().Debug("Getting saga with {sagaId}",correlationId);
            try
            {
                using (var db = await _getDb.CreateAsync(CancellationToken.None))
                {
                  var st=  await db.QueryRowAsync(d=>d.From<SagasRow>().Where(c=>c.Id==correlationId).SelectAll(),CancellationToken.None).ConfigureFalse();
                    if (st != null)
                    {
                        return  new SagaState()
                        {
                            Id=correlationId,Version = st.Version,Data = JsonSerializer.Deserialize<Dictionary<string, object>>(st.Data)
                        };
                    }
                    
                    return null;
                }
            }
            catch (DbException ex)
            {
                throw  new MonolithBusStorageException(ex);
            }
        }

        public async Task Save(SagaState state, bool isNew = false)
        {
            var packed = JsonSerializer.ToJsonString(state.Data);
            try
            {
                using (var db = await _getDb.CreateAsync(CancellationToken.None))
                {
                    if (isNew)
                    {
                        await db.InsertIgnoreAsync(new SagasRow()
                        {
                            Id = state.Id, Data = packed, Version = 0
                        }).ConfigureFalse();
                        return;
                    }

                    var old = state.Version;
                    state.Version++;
                    var count=await db.Update<SagasRow>().Set(d => d.Data, packed).Set(d => d.Version, state.Version)
                        .Where(d => d.Id == state.Id && d.Version == old).ExecuteAsync().ConfigureFalse();
                    if (count==0) throw new SagaConcurrencyException();
                    this.Log().Debug("Saving saga with {sagaId}",state.Id);
                }
            }
            catch (DbException ex)
            {
                throw  new MonolithBusStorageException(ex);
            }
            
        }

        public async Task Delete(string id)
        {
            try
            {
                using (var db = await _getDb.CreateAsync(CancellationToken.None))
                {
                    await db.DeleteFromAsync<SagasRow>(criteria: d => d.Id == id).ConfigureFalse();
                    this.Log().Debug("Saga with {sagaId} completed. Data deleted.",id);
                }
            }
            catch (DbException ex)
            {
                throw  new MonolithBusStorageException(ex);
            }
        }

        public async Task<Guid[]> Get(ReservedIdsSource input)
        {
            Guid[] UnpackIds(string s) => s.Split(',').Select(Guid.Parse).ToArray();
            try
            {
                using (var db = await _getDb.CreateAsync(CancellationToken.None))
                {
                    var r=await db.QueryValueAsync(d=>d.From<ReservedIdsRow>().Where(c=>c.SourceId==input.GetId()).Select(f=>f.Ids))
                        .ConfigureFalse();
                    if (!r.IsNullOrEmpty())
                    {
                        return UnpackIds(r);
                    }

                    return Array.Empty<Guid>();
                }
            }
            catch (DbException ex)
            {
                throw  new MonolithBusStorageException(ex);
            }
        }

        

        public async Task Add(ReservedIdsSource id, Guid[] ids)
        {
            string PackIds(Guid[] i) => i.Select(d => d.ToString()).StringJoin();
            
            try
            {
                using (var db = await _getDb.CreateAsync(CancellationToken.None))
                {
                    await db.InsertIgnoreAsync(new ReservedIdsRow() {SourceId = id.GetId(), Ids = PackIds(ids)})
                        .ConfigureFalse();
                }
            }
            catch (DbException ex)
            {
                throw  new MonolithBusStorageException(ex);
            }
        }

        public async Task<IEnumerable<IIdentifyMessage>> GetUnhandledMessages()
        {
            try
            {
                using (var db = await _getDb.CreateAsync(CancellationToken.None))
                {
                    var r=await db.QueryAsAsync(d => d.From<MessagesRow>().Where(f=>!f.IsPoison).OrderBy(c=>c.Id).Select(t => new {t.Data, t.MessageType}),CancellationToken.None).ConfigureFalse();
                    return r.Select(d => UnpackMessage(d.Data, Type.GetType(d.MessageType))).ToArray();

                }
            }
            catch (DbException ex)
            {
                throw  new MonolithBusStorageException(ex);
            }
        }

        public async Task MarkMessageHandled(Guid id)
        {
           
            try
            {
                using (var db = await _getDb.CreateAsync(CancellationToken.None))
                {
                    await db.DeleteFromAsync<MessagesRow>(criteria: d => d.MessageId == id).ConfigureFalse();
                }
                this.Log().Debug("Message with id {messageId} marked as handled",id);
            }
            catch (DbException ex)
            {
                throw  new MonolithBusStorageException(ex);
            }
        }

        public async Task MarkMessageFailed(Guid id)
        {
            try
            {
                using (var db = await _getDb.CreateAsync(CancellationToken.None))
                {
                    await db.Update<MessagesRow>()
                            .Set(d=>d.IsPoison,true)
                        .Where(f=>f.MessageId==id)
                            .ExecuteAsync()
                        ;
                }
            }
            catch (DbException ex)
            {
                throw  new MonolithBusStorageException(ex);
            }
        }

        internal const string Table_Messages = "mbus_Messages";
        internal const string Table_Sagas = "mbus_Sagas";
        internal const string Table_Reserved = "mbus_Idempotency";

        internal static void SetupSqlFu(IDbProvider provider,string cnx)
        {
            SqlFuManager.Configure(c =>
            {
                c.AddProfile(provider,cnx,"monolithBus");
                c.ConfigureTableForPoco<MessagesRow>(t =>
                {
                    t.TableName = Table_Messages;
                    t.Property(f => f.Id).IsAutoincremented();
                });
                c.ConfigureTableForPoco<SagasRow>(t => { t.TableName = Table_Sagas; });
                c.ConfigureTableForPoco<ReservedIdsSource>(t => { t.TableName = Table_Reserved; });
            });
        }

        internal void InitSqlite()
        {
            Init($@"
create table if not exists {SqlStorage.Table_Messages}(
Id integer not null primary key,
MessageId text not null,
Data text not null,
MessageType text not null,
IsPoison boolean not null
);

create unique index if not exists Ix_Msg on {SqlStorage.Table_Messages}(MessageId);

create table if not exists {SqlStorage.Table_Sagas}(
Id text not null,
Data text not null,
Version integer not null
);

create unique index if not exists Ix_Saga on  {SqlStorage.Table_Sagas}(Id);

create table if not exists {SqlStorage.Table_Reserved}(
SourceId text not null unique,
Ids text not null
);

");
        }
    }
}