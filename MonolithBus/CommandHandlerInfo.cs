﻿using System.Reflection;
using System.Threading.Tasks;
using CavemanTools.Infrastructure;

namespace MonolithBus
{
    public class CommandHandlerInfo:MessageHandlerInfo
    {
        private CommandHandlerInfo()
        {
            Category=MonolithBus.MessageType.Command;
        }

        public bool IsRequest { get; private set; }

        public static CommandHandlerInfo From(MethodInfo mi)
        {
            var i=new CommandHandlerInfo();
            FillInInfo(mi,i);
            i.IsRequest = mi.ReturnType == typeof(Task<CommandResult>);
            return i;
        }
    }
}