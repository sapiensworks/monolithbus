﻿using System;
using System.Threading.Tasks;

namespace MonolithBus
{
    /// <summary>
    /// Used as a singleton, must be thread safe
    /// </summary>
    public interface IStoreReservedMessagesIds
    {
        Task<Guid[]> Get(ReservedIdsSource input);
        /// <summary>
        /// ignore duplicates
        /// </summary>
        /// <param name="id"></param>
        /// <param name="ids"></param>
        /// <returns></returns>
        Task Add(ReservedIdsSource id,Guid[] ids);      
    }
}