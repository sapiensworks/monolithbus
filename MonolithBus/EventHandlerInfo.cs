﻿using System;
using System.Reflection;

namespace MonolithBus
{
    public class EventHandlerInfo : MessageHandlerInfo
    {
        public bool IsSaga { get; private set; }
        public bool StartsSaga { get; private set; }

        private EventHandlerInfo()
        {
            Category=MonolithBus.MessageType.Event;
        }

        public static EventHandlerInfo From(MethodInfo mi)
        {
            var i=new EventHandlerInfo();
            FillInInfo(mi,i);
            i.IsSaga = mi.DeclaringType.DerivesFrom<ASaga>();
            i.StartsSaga = mi.HasCustomAttribute<StartsSagaAttribute>();
            return i;
        }
    }
}