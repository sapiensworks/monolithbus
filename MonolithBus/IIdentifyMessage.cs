﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using CavemanTools.Infrastructure;

namespace MonolithBus
{
    public interface  IIdentifyMessage
    {
        Guid MessageId { get;  }
    }

    public class MessageHandlerAttribute : Attribute
    {

    }

    public class StartsSagaAttribute : Attribute
    {

    }


    public class RelayAttribute : Attribute
    {

    }

    public abstract class AMessage : IIdentifyMessage
    {
        public Guid MessageId { get; set; } = Guid.NewGuid();
        public DateTimeOffset Timestamp { get; set; }=DateTimeOffset.Now;
    }

    public interface IDispatchMessages
    {
        /// <summary>
        /// Executes the command synchronously and returns a command result.
        /// Command is still persisted and automatically sent again if app crashes
        /// </summary>
        /// <param name="cmd"></param>
        /// <returns></returns>
        Task<CommandResult> Request(IIdentifyMessage cmd);

        /// <summary>
        /// Command will be queued for execution on a dedicated background task.
        /// There is only one commands queue, command execution is serial
        /// </summary>
        /// <param name="cmd"></param>
        /// <returns></returns>
        Task Send(params IIdentifyMessage[] cmd);
        /// <summary>
        /// Events will be queued for execution on a background task
        /// There is only one events queue, events execution is serial
        /// </summary>
        /// <param name="events"></param>
        /// <returns></returns>
        Task Publish(params IIdentifyMessage[] events);

        /// <summary>
        ///  Use it to reserve commands ids when you need to send one from an event handler.
        ///  This way, you'll get the same id for the commands which helps to prevent duplicate commands
        ///  </summary>
        /// <param name="handlerType">The type invoking the reservation</param>
        /// <param name="msgId">Event id </param>
        /// <param name="howMany">How many ids should be reserved</param>
        /// <returns></returns>
        Task<Guid[]> ReserveIdsFor<T>(T handlerType, Guid msgId, int howMany);
    }


    //public interface IConfigureMessageIdentification
    //{
    //    IConfigureMessageIdentification CommandsLookLike(Func<Type,bool> cfg);
    //    IConfigureMessageIdentification EventsLookLike(Func<Type,bool> cfg);
    //}

    

    

    //public interface IConfigureSimpleBus
    //{
    //    IConfigureSimpleBus AddHandlers(IEnumerable<Type> types, Action<IConfigureMessageIdentification> cfg);
    //    IConfigureSimpleBus WithStorage(IAddMessageToProcessorStorage storage);
    //}

    //public class Dispatcher
    //{
    //    private readonly IAddMessageToProcessorStorage _storage;

    //    public Dispatcher(IAddMessageToProcessorStorage storage)
    //    {
    //        _storage = storage;
    //    }


    //}


    //public interface IChooseProcessor
    //{
    //    IProcessMessages GetProcessor(Type msg);
    //}

    //public interface IProcessMessages
    //{
    //    void Process(IIdentifyMessage msg);
    //}


}