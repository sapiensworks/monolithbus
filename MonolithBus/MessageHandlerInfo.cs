﻿using System;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using CavemanTools.Infrastructure;

namespace MonolithBus
{


    public class MessageHandlerInfo
    {
        public Type HandlerType { get; private set; }
        public Type MessageType { get; private set; }
        public MessageType Category { get; set; }

        public MessageHandlerInfo()
        {
            
        }
      
        protected static void FillInInfo<T>(MethodInfo mi, T inst) where T:MessageHandlerInfo
        {
            inst.HandlerType=mi.DeclaringType;
            inst.MessageType=mi.GetParameters()[0].ParameterType;
        }

        class HandlerInfo
        {
            public string MethodName;
            public Type resultType;
           
            public Func<MethodInfo, MessageHandlerInfo> ExtractInfo;

            public HandlerInfo(string name, Type resultType, Func<MethodInfo, MessageHandlerInfo> extractInfo)
            {
                MethodName = name;
                this.resultType = resultType;
              
                ExtractInfo = extractInfo;
            }
        }

        public static Optional<MessageHandlerInfo> GetHandlerInfo(MethodInfo info)
        {
            var args = info.GetParameters();
            if (args.Length!=1) return Optional<MessageHandlerInfo>.Empty;

            var hi = _handlerIdInfo.FirstOrDefault(d => d.MethodName == info.Name && info.ReturnType == d.resultType);
            if (hi==null) return Optional<MessageHandlerInfo>.Empty;

            return hi.ExtractInfo(info);
        }

        private static HandlerInfo[] _handlerIdInfo = new[]
        {
            new HandlerInfo("Execute",typeof(Task), CommandHandlerInfo.From), 
            new HandlerInfo("Handle",typeof(Task),EventHandlerInfo.From),             
            new HandlerInfo("Execute",typeof(Task<CommandResult>),CommandHandlerInfo.From),             
        };
    }

    public enum MessageType
    {
        None,Command, Event
    }
}